defmodule Identity do
  @moduledoc """
  Documentation for Identity.
  """
  alias Identity.{Repo, User, Encryption}

  @doc """
  Creates a new user.
  """
  @spec create_user(map) :: {:ok, User.t} | {:error, map}
  def create_user(params) do
    params
    |> User.new_changeset()
    |> Repo.insert()
    |> case do
      {:ok, schema} ->
        {:ok, schema}
      {:error, changeset} ->
        {:error, errors_to_map(changeset)}
    end
  end

  @doc """
  Retrieves a user by a username.
  """
  @spec lookup_user(String.t) :: User.t | nil
  def lookup_user(username) do
    Repo.get_user_by_username(username)
  end

  @doc """
  Attempts to authenticate as a user.
  """
  @spec login(String.t, String.t) :: {:ok, User.t} | :error
  def login(username, password) do
    username
    |> lookup_user()
    |> maybe_check_password(password)
  end

  defp maybe_check_password(nil, _password), do: :error
  defp maybe_check_password(%User{} = user, password) do
    case Encryption.check_password(password, user.hashed_password) do
      true -> {:ok, user}
      false -> :error
    end
  end

  @doc """
  Shortcut to `Identity.Encryption.check_password/2`
  """
  @spec check_password(User.t, String.t) :: true | false
  def check_password(user, password), do: Encryption.check_password(password, user.hashed_password)

  @doc """
  Verifies the email for a user.
  """
  @spec verify_email(User.t, String.t) :: User.t | :error
  def verify_email(%User{verified_email: true} = user, _verification_token) do
    user
  end
  def verify_email(%User{email_verification_token: token} = user, token) do
    do_update!(:verify_email_changeset, [user])
  end
  def verify_email(_user, _verification_token) do
    :error
  end

  @doc """
  Changes the email for a user.

  Expects a map with a new email and a confirmation field.
  """
  @spec change_email(User.t, map) :: {:ok, User.t} | {:error, map}
  def change_email(user, params) do
    do_update(:change_email_changeset, [user, params])
  end

  @doc """
  Changes the password for a user.

  Expects a map with a new password and a confirmation field.
  """
  @spec change_password(User.t, map) :: {:ok, User.t} | {:error, map}
  def change_password(user, params) do
    do_update(:change_password_changeset, [user, params])
  end

  @doc """
  Changes the password for a user if the provided password is valid.

  Expects a map with a new password and a confirmation field.
  """
  @spec checked_change_password(User.t, String.t, map) :: {:ok, User.t} | {:error, map}
  def checked_change_password(user, password, params) do
    check_password_before(user, password, :change_password, [user, params])
  end

  @doc """
  Changes the email for a user if the provided password is valid.

  Expects a map with a new email and a confirmation field.
  """
  @spec checked_change_email(User.t, String.t, map) :: {:ok, User.t} | {:error, map}
  def checked_change_email(user, password, params) do
    check_password_before(user, password, :change_email, [user, params])
  end

  defp check_password_before(user, password, action, params) do
    case check_password(user, password) do
      true -> apply(__MODULE__, action, params)
      false -> {:error, %{current_password: ["is invalid"]}}
    end
  end

  @doc """
  Updates the permissions for a user.
  """
  @spec update_permissions(User.t, map) :: {:ok, User.t} | {:error, map}
  def update_permissions(user, params) do
    do_update(:update_permissions_changeset, [user, params])
  end

  @doc """
  Deletes the user.
  """
  @spec delete_user(User.t, String.t) :: :ok | :error
  def delete_user(user, password) do
    case Encryption.check_password(password, user.hashed_password) do
      true ->
        Repo.delete!(user)
        :ok
      false -> :error
    end
  end

  @doc """
  Retrieves a user by their id.
  """
  @spec get_user(integer) :: User.t | nil
  def get_user(id) do
    Repo.get(User, id)
  end

  defp do_update!(action, params) do
    apply(User, action, params)
    |> Repo.update!()
  end

  defp do_update(action, params) do
    apply(User, action, params)
    |> Repo.update()
    |> case do
      {:ok, schema} ->
        {:ok, schema}
      {:error, changeset} ->
        {:error, errors_to_map(changeset)}
    end
  end

  defp errors_to_map(changeset) do
    Ecto.Changeset.traverse_errors(changeset, fn {msg, opts} ->
      Enum.reduce(opts, msg, fn {key, value}, acc ->
        String.replace(acc, "%{#{key}}", to_string(value))
      end)
    end)
  end
end
