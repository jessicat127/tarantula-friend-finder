defmodule Identity.Encryption do
  @moduledoc """
  General purpose encryption.
  """
  @comeonin_mod Application.get_env(:identity, :comeonin_mod)
  @hash_function Application.get_env(:identity, :hash_function)

  @doc """
  Hashes a password.
  """
  @spec hash_password(String.t) :: String.t
  def hash_password(password) do
    :crypto.hash(@hash_function, password)
    |> @comeonin_mod.hashpwsalt()
  end

  @doc """
  Checks a password against a hash in constant time.
  """
  @spec check_password(String.t, String.t) :: boolean
  def check_password(password, hash) do
    :crypto.hash(@hash_function, password)
    |> @comeonin_mod.checkpw(hash)
  end
end
