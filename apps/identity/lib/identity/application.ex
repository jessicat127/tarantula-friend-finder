defmodule Identity.Application do
  @moduledoc false
  use Application
  alias Identity.Repo

  def start(_type, _args) do
    import Supervisor.Spec, warn: false

    System.get_env("ENVIRONMENT")
    |> configure_environment()

    children = [
      supervisor(Repo, []),
    ]

    opts = [strategy: :rest_for_one, name: Identity.Supervisor]
    Supervisor.start_link(children, opts)
  end

  defp configure_environment(nil), do: :ok
  defp configure_environment(env) do
    username = System.get_env("DB_USERNAME")
    password = System.get_env("DB_PASSWORD")
    hostname = System.get_env("DB_HOSTNAME")
    database = System.get_env("DB_NAME")

    require Logger
    Logger.info("Configuring environment", [
      username: username,
      hostname: hostname,
      database: database,
      module: Identity.Repo
    ])

    old_config = Application.get_env(:identity, Identity.Repo)
    new_config = Keyword.merge(old_config, [
      username: username,
      password: password,
      hostname: hostname,
      database: database
    ])
    Elixir.Application.put_env(:identity, Identity.Repo, new_config, [persistent: true])
  end
end
