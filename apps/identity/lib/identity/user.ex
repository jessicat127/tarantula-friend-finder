defmodule Identity.User do
  @moduledoc """
  Ecto schema for a user.
  """
  use Ecto.Schema
  use Identity.Changeset
  alias Ecto.Changeset

  @timestamps_opts [type: :utc_datetime]

  @type t :: %__MODULE__{}

  schema "users" do
    field :username, :string
    field :email, :string
    field :password, :string, virtual: true
    field :hashed_password, :string
    field :verified_email, :boolean, default: false
    field :email_verification_token, :string
    field :default_permissions, :integer, default: 0
    field :user_permissions, :integer, default: 0
    field :admin_permissions, :integer, default: 0

    timestamps()
  end

  @doc """
  Creates a changeset for a new user based on a set of params.
  """
  @spec new_changeset(map) :: Changeset.t
  def new_changeset(params) do
    %__MODULE__{}
    |> cast(params, [:username, :email, :password, :default_permissions, :user_permissions, :admin_permissions])
    |> put_change(:email_verification_token, new_verification_token())
    |> validate_required([:username, :email, :password])
    |> validate_email(:email)
    |> validate_length(:password, min: 10)
    |> validate_confirmation(:password, required: true)
    |> validate_confirmation(:email, required: true)
    |> hash_password_field()
    |> validate_number(:default_permissions, greater_than_or_equal_to: 0)
    |> validate_number(:user_permissions, greater_than_or_equal_to: 0)
    |> validate_number(:admin_permissions, greater_than_or_equal_to: 0)
    |> unique_constraint(:username)
    |> unique_constraint(:email)
  end

  @doc """
  Creates a changeset that marks a user's email as verified.
  """
  @spec verify_email_changeset(t) :: Changeset.t
  def verify_email_changeset(user) do
    user
    |> cast(%{verified_email: true, email_verification_token: nil}, [:verified_email, :email_verification_token])
  end

  @doc """
  Creates a changeset for changing a user's password.
  A confirmation parameter `:password_confirmation` is expected.
  """
  @spec change_password_changeset(t, map) :: Changeset.t
  def change_password_changeset(user, confirmed_pwd) do
    user
    |> cast(confirmed_pwd, [:password])
    |> validate_required([:password])
    |> validate_new_password()
    |> validate_length(:password, min: 10)
    |> validate_confirmation(:password, required: true)
    |> hash_password_field()
  end

  @doc """
  Creates a changeset for changing a user's email.
  A confirmation parameter `:email_confirmation` is expected.
  """
  @spec change_email_changeset(t, map) :: Changeset.t
  def change_email_changeset(user, confirmed_email) do
    user
    |> cast(confirmed_email, [:email])
    |> put_change(:verified_email, false)
    |> put_change(:email_verification_token, new_verification_token())
    |> validate_required([:email])
    |> validate_confirmation(:email, required: true)
    |> validate_email(:email)
  end

  @doc """
  Creates a changeset for updating a user's permissions.
  """
  @spec update_permissions_changeset(t, map) :: Changeset.t
  def update_permissions_changeset(user, params) do
    user
    |> cast(params, [:default_permissions, :user_permissions, :admin_permissions])
    |> validate_number(:default_permissions, greater_than_or_equal_to: 0)
    |> validate_number(:user_permissions, greater_than_or_equal_to: 0)
    |> validate_number(:admin_permissions, greater_than_or_equal_to: 0)
  end

  @verification_token_bytes 64

  defp new_verification_token do
    :crypto.strong_rand_bytes(@verification_token_bytes)
    |> Base.encode64()
  end
end
