defmodule Identity.Repo.Migrations.MoreUserPermissions do
  use Ecto.Migration

  def change do
    alter table(:users) do
      remove :permissions#, :integer, [null: false, default: 0]
      add :default_permissions, :integer, [null: false, default: 0]
      add :user_permissions, :integer, [null: false, default: 0]
      add :admin_permissions, :integer, [null: false, default: 0]
    end
  end
end
