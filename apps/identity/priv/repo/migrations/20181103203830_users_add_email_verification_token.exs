defmodule Identity.Repo.Migrations.UsersAddEmailVerificationToken do
  use Ecto.Migration

  def change do
    alter table(:users) do
      add :email_verification_token, :string
    end
  end
end
