# Since configuration is shared in umbrella projects, this file
# should only configure the :tarantula_friend_finder application itself
# and only for organization purposes. All other config goes to
# the umbrella root.
use Mix.Config

# Configure your database
config :identity, Identity.Repo,
  username: "postgres",
  password: "postgres",
  database: "identity_test",
  hostname: "localhost",
  pool: Ecto.Adapters.SQL.Sandbox

config :bcrypt_elixir, log_rounds: 4

