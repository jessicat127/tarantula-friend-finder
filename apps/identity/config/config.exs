# Since configuration is shared in umbrella projects, this file
# should only configure the :tarantula_friend_finder application itself
# and only for organization purposes. All other config goes to
# the umbrella root.
use Mix.Config

config :identity, ecto_repos: [Identity.Repo]

config :identity, Repo,
  adapter: Ecto.Adapters.Postgres,
  database: "identity",
  migration_timestamps: [type: :utc_datetime]

config :identity,
  comeonin_mod: Comeonin.Bcrypt,
  hash_function: :sha256

import_config "#{Mix.env}.exs"



