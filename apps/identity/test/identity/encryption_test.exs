defmodule Identity.EncryptionTest do
  use ExUnit.Case
  doctest Identity.Encryption

  test "matching passwords return true" do
    hash = Identity.Encryption.hash_password("password")
    assert Identity.Encryption.check_password("password",hash) == true
  end

  test "different passwords return false" do
    hash = Identity.Encryption.hash_password("password1")
    assert Identity.Encryption.check_password("password2",hash) == false
  end
end
