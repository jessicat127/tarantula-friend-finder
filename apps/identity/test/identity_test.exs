defmodule IdentityTest do
  use ExUnit.Case
  doctest Identity
  alias Identity.{User, Repo, Encryption}

  setup do
    :ok = Ecto.Adapters.SQL.Sandbox.checkout(Repo)
  end

  describe "create_user/1" do
    test "can create user with valid params" do
      result =
        rand_user_params()
        |> Identity.create_user()

      assert match?({:ok, %User{}}, result)
    end

    test "can't create user with bad params" do
      result =
        %{password_confirmation: "doesnt_match"}
        |> rand_user_params()
        |> Identity.create_user()

      assert match?({:error, %{}}, result)
    end
  end

  describe "lookup_user/1" do
    test "can lookup user with existing username" do
      user = new_user()
      user2 = Identity.lookup_user(user.username)

      assert user.id === user2.id
    end

    test "get nil when looking up non-existent user" do
      result = Identity.lookup_user("noone")

      assert is_nil(result)
    end
  end

  describe "login/1" do
    test "can login with correct password" do
      password = "password123"
      user = new_user(%{password: password, password_confirmation: password})
      result = Identity.login(user.username, password)

      assert match?({:ok, %User{}}, result)
    end

    test "can't login with bad password" do
      user = new_user()
      result = Identity.login(user.username, "wrong_password")

      assert match?(:error, result)
    end
  end

  describe "check_password/2" do
    test "correct password evaluates to true" do
      password = "password123"
      user = new_user(%{password: password, password_confirmation: password})
      result = Identity.check_password(user, password)

      assert result
    end

    test "bad password evaluates to false" do
      user = new_user()
      result = Identity.check_password(user, "wrong_password")

      assert !result
    end
  end

  describe "verify_email/2" do
    test "can mark user as having a verified email" do
      user = new_user()
      user2 = Identity.verify_email(user, user.email_verification_token)

      assert user2.verified_email
      assert is_nil(user2.email_verification_token)
    end

    test "can't verify email if token doesn't match" do
      user = new_user()
      result = Identity.verify_email(user, "dsoinfsd")

      assert match?(:error, result)
    end
  end

  describe "change_email/2" do
    test "can change email with good params" do
      user = new_user()
      new_email = "nsa@us.gov"
      {:ok, user2} = Identity.change_email(user, %{email: new_email, email_confirmation: new_email})

      assert user2.email === new_email
      assert (not user2.verified_email)
    end

    test "can't change email with bad params" do
      user = new_user()
      result = Identity.change_email(user, %{email: "nsa@us.gov"})

      assert match?({:error, %{}}, result)
    end
  end

  describe "change_password/2" do
    test "can change password with good params" do
      user = new_user()
      new_password = "secret_password"
      {:ok, user2} = Identity.change_password(user, %{password: new_password, password_confirmation: new_password})

      assert Encryption.check_password(new_password, user2.hashed_password)
    end

    test "can't change password with bad params" do
      user = new_user()
      result = Identity.change_password(user, %{password: "blahblah"})

      assert match?({:error, %{}}, result)
    end
  end

  describe "checked_change_password/3" do
    test "can change password with correct password" do
      password = "password123"
      user = new_user(%{password: password, password_confirmation: password})
      new_password = "secret_password"
      {:ok, user2} = Identity.checked_change_password(user, password, %{password: new_password, password_confirmation: new_password})

      assert Encryption.check_password(new_password, user2.hashed_password)
    end

    test "can't change password with incorrect password" do
      user = new_user()
      result = Identity.checked_change_password(user, "abcd", %{password: "blahblah"})

      assert match?({:error, %{}}, result)
    end
  end

  describe "update_permissions/2" do
    test "can update permissions with a good value" do
      user = new_user()
      perms = %{
        default_permissions: 0b1,
        user_permissions: 0b11,
        admin_permissions: 0b1
      }
      {:ok, user2} = Identity.update_permissions(user, perms)

      assert user2.default_permissions === perms.default_permissions
      assert user2.user_permissions === perms.user_permissions
      assert user2.admin_permissions === perms.admin_permissions
    end

    test "can't update permissions with a bad value" do
      user = new_user()
      perms = %{
        default_permissions: -1,
        user_permissions: -1,
        admin_permissions: -1
      }
      result = Identity.update_permissions(user, perms)

      assert match?({:error, %{}}, result)
    end
  end

  describe "delete_user/2" do
    test "can delete user if provided a correct password" do
      password = "mypassword"
      user = new_user(%{password: password, password_confirmation: password})
      result = Identity.delete_user(user, password)
      user2 = Identity.lookup_user(user.username)

      assert match?(:ok, result)
      assert is_nil(user2)
    end

    test "can't delete user with wrong password" do
      user = new_user()
      result = Identity.delete_user(user, "wrong_password")
      user2 = Identity.lookup_user(user.username)

      assert match?(:error, result)
      assert user.id === user2.id
    end

    test "trying to delete non-existent user throws" do
      password = "mypassword"
      user = new_user(%{password: password, password_confirmation: password})
      :ok = Identity.delete_user(user, password)

      assert_raise Ecto.StaleEntryError, fn ->
        Identity.delete_user(user, password)
      end
    end
  end

  describe "get_user/1" do
    test "can get user with an id" do
      user = new_user()
      user2 = Identity.get_user(user.id)

      assert user.id === user2.id
    end

    test "get nil for non-existent user id" do
      result = Identity.get_user(123)

      assert is_nil(result)
    end
  end

  defp new_user(params \\ %{}) do
    {:ok, user} =
      params
      |> rand_user_params()
      |> Identity.create_user()
    user
  end

  defp rand_user_params(params \\ %{}) do
    email = rand_string() <> "@gmail.com"
    password = rand_string()
    %{
      username: rand_string(),
      email: email,
      email_confirmation: email,
      password: password,
      password_confirmation: password
    } |> Map.merge(params)
  end

  def rand_string do
    :crypto.strong_rand_bytes(32)
    |> Base.encode64()
  end
end
