defmodule TarantulaFriendFinder.Repo do
  use Ecto.Repo,
    otp_app: :tarantula_friend_finder,
    adapter: Ecto.Adapters.Postgres

end
