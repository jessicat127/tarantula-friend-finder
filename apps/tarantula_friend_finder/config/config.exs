# Since configuration is shared in umbrella projects, this file
# should only configure the :tarantula_friend_finder application itself
# and only for organization purposes. All other config goes to
# the umbrella root.
use Mix.Config

config :tarantula_friend_finder, ecto_repos: [TarantulaFriendFinder.Repo]

config :tarantula_friend_finder, Repo,
  adapter: Ecto.Adapters.Postgres,
  database: "tarantula_field_finder",
  migration_timestamps: [type: :utc_datetime]

import_config "#{Mix.env}.exs"


