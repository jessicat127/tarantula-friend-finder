# Since configuration is shared in umbrella projects, this file
# should only configure the :tarantula_friend_finder application itself
# and only for organization purposes. All other config goes to
# the umbrella root.
use Mix.Config

# Configure your database
config :tarantula_friend_finder, TarantulaFriendFinder.Repo,
  username: "postgres",
  password: "postgres",
  database: "tarantula_friend_finder_test",
  hostname: "localhost",
  pool: Ecto.Adapters.SQL.Sandbox
