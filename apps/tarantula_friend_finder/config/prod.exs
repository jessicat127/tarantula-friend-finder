# Since configuration is shared in umbrella projects, this file
# should only configure the :tarantula_friend_finder application itself
# and only for organization purposes. All other config goes to
# the umbrella root.
use Mix.Config

config :tarantula_friend_finder, TarantulaFriendFinder.Repo,
  username: "",
  password: "",
  database: "tarantula_friend_finder_prod",
  pool_size: 15

#import_config "prod.secret.exs"
