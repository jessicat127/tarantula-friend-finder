ExUnit.start()

Ecto.Adapters.SQL.Sandbox.mode(Identity.Repo, :manual)
#Ecto.Adapters.SQL.Sandbox.mode(TarantulaFriendFinder.Repo, :manual)

# Add Swagger schema validation to tests
# https://hexdocs.pm/phoenix_swagger/test-helpers.html
