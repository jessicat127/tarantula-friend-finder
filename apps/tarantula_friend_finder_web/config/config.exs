# Since configuration is shared in umbrella projects, this file
# should only configure the :tarantula_friend_finder_web application itself
# and only for organization purposes. All other config goes to
# the umbrella root.
use Mix.Config

# General application configuration
config :tarantula_friend_finder_web,
  ecto_repos: [Identity.Repo], #TarantulaFriendFinder.Repo, Identity.Repo],
  generators: [context_app: :tarantula_friend_finder]

# Configures the endpoint
config :tarantula_friend_finder_web, TarantulaFriendFinderWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "aTc2Osnllur81hlF3SO9UNvkC6A2FKxOiWcOLwYGZ5P/WPdqy+xnLIr5/hEq1Zem",
  render_errors: [view: TarantulaFriendFinderWeb.ErrorView, accepts: ~w(html json)],
  pubsub: [name: TarantulaFriendFinderWeb.PubSub,
           adapter: Phoenix.PubSub.PG2]

config :phoenix, :format_encoders,
  json: Jason

config :phoenix, :template_engines,
  drab: Drab.Live.Engine

config :drab, TarantulaFriendFinderWeb.Endpoint,
  js_socket_constructor: "window.__socket"

config :drab, TarantulaFriendFinderWeb.Endpoint,
  otp_app: :tarantula_friend_finder_web

config :drab, :modal_css, :bootstrap4

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env}.exs"

config :logger, :console,
  level: :info
