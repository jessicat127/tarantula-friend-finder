# Since configuration is shared in umbrella projects, this file
# should only configure the :tarantula_friend_finder_web application itself
# and only for organization purposes. All other config goes to
# the umbrella root.
use Mix.Config

# We don't run a server during test. If one is required,
# you can enable the server option below.
config :tarantula_friend_finder_web, TarantulaFriendFinderWeb.Endpoint,
  http: [port: 4001],
  server: false

config :tarantula_friend_finder_web, TarantulaFriendFinderWeb.Authentication.Guardian,
  issuer: "tarantula_friend_finder_web",
  secret_key: "LUkf8no9vcnT4+2xIF/R5Vn6QyMKSJrnH6TvpOzctfYRzvUKW7tPxc3eCKf1esYz"

config :bcrypt_elixir, log_rounds: 4
