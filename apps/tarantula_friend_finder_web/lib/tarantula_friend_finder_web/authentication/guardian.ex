defmodule TarantulaFriendFinderWeb.Authentication.Guardian do
  use Guardian, otp_app: :tarantula_friend_finder_web,
    permissions: %{
      default: [:public_profile],
      user_actions: [:create_listing, :delete_listing],
      admin_actions: [
        :add_genus,
        :delete_genus,
        :add_species,
        :delete_species,
        :set_permissions,
        :list_permissions
      ]
    }

  use Guardian.Permissions.Bitwise

  def subject_for_token(%Identity.User{id: uid} = _user, _claims) do
    {:ok, "User:#{uid}"}
  end

  def subject_for_token(_, _), do: {:error, :unhandled_resource_type}

  def resource_from_claims(%{"sub" => "User:" <> uid}) do
    case Identity.get_user(uid) do
      nil -> {:error, :user_not_found}
      user -> {:ok, user}
    end
  end

  def resource_from_claims(_), do: {:error, :unhandled_resource_type}

  def build_claims(claims, _resource, opts) do
    perms = Keyword.get(opts, :permissions)
    new_claims = encode_permissions_into_claims!(claims, perms)
    {:ok, new_claims}
  end


# How to use permissions as plugs:

# Ensure that both the `public_profile` and `user_actions.books` permissions are present in the token
# plug Guardian.Permissions.Bitwise, ensure: %{default: [:public_profile], user_actions: [:books]}

# Allow the request to continue when the token contains any of the permission sets specified
# plug Guardian.Permissions.Bitwise, one_of: [
#  %{default: [:public_profile], user_actions: [:books]},
#  %{default: [:public_profile], user_actions: [:music]},
#]

# Look for permissions for a token in a different location
# plug Guardian.Permissions.Bitwise, key: :impersonate, ensure: %{default: [:public_profile]}
end
