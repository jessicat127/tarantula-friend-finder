defmodule TarantulaFriendFinderWeb.Authentication.ErrorHandlers.HTML do
  import Plug.Conn

  def auth_error(conn, {type, _reason}, _opts) do
    body = to_string(type)
    conn
    |> Phoenix.Controller.put_view(TarantulaFriendFinderWeb.ErrorView)
    |> put_status(401)
    |> Phoenix.Controller.render("401.html", body: body)
  end
end
