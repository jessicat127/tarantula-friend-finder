defmodule TarantulaFriendFinderWeb.Router do
  use TarantulaFriendFinderWeb, :router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    #plug :fetch_flash
    plug :protect_from_forgery
    plug :put_secure_browser_headers
  end

  pipeline :auth do
    plug TarantulaFriendFinderWeb.Authentication.Pipeline
  end

  pipeline :ensure_auth do
    plug Guardian.Plug.EnsureAuthenticated
  end

  pipeline :ensure_not_auth do
    plug Guardian.Plug.EnsureNotAuthenticated
  end

  pipeline :auth_html_errors do
    plug Guardian.Plug.Pipeline, error_handler: AuthMe.UserManager.ErrorHandlers.HTML
  end

  pipeline :auth_json_errors do
    plug Guardian.Plug.Pipeline, error_handler: AuthMe.UserManager.ErrorHandlers.JSON
  end

  # BROWSER

  # definitely logged in scope
  scope "/", TarantulaFriendFinderWeb do
    pipe_through [:browser, :auth, :ensure_auth, :auth_html_errors]

    scope "/auth" do
      get "/test", AuthController, :test
      get "/logout", AuthController, :logout
    end

    get "/users/edit", UserController, :edit
  end

  # definitely not logged in scope
  scope "/", TarantulaFriendFinderWeb do
    pipe_through [:browser, :auth, :ensure_not_auth, :auth_html_errors]

    scope "/auth" do
      post "/", AuthController, :authenticate
      get "/login", AuthController, :login
    end

    get "/users/new", UserController, :new
  end

  # maybe logged in scope
  scope "/", TarantulaFriendFinderWeb do
    pipe_through [:browser, :auth, :auth_html_errors]

    get "/", SearchController, :index

    get "/users/:id", UserController, :show
  end
end
