defmodule TarantulaFriendFinderWeb.UserController do
  use TarantulaFriendFinderWeb, :controller

  def show(conn, %{"id" => id } = params) do
    render(conn, "show.html", [])
  end

  def new(conn, params) do
    render(conn, "new.html", [
      username_feedback: nil,
      password_feedback: nil,
      password_confirmation_feedback: nil,
      email_feedback: nil,
      email_confirmation_feedback: nil,
      loading: false
      ])
  end

  def edit(conn, params) do
    render(conn, "edit.html", [edit_partial: :change_password])
  end
end
