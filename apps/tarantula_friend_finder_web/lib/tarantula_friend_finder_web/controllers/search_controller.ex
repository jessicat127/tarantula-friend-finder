defmodule TarantulaFriendFinderWeb.SearchController do
  use TarantulaFriendFinderWeb, :controller

  def index(conn, params) do
    render(conn, "index.html", [form_partial: nil])
  end
end
