defmodule TarantulaFriendFinderWeb.AuthController do
  use TarantulaFriendFinderWeb, :controller
  alias TarantulaFriendFinderWeb.Authentication, as: Auth

  def authenticate(conn, params) do
    username = params["username"]
    password = params["password"]
    Identity.login(username, password)
    |> authenticate_reply(conn)
  end

  defp authenticate_reply({:ok, user}, conn) do
    conn
    |> Guardian.Plug.sign_in(Auth.Guardian, user)
    |> redirect(to: Routes.search_path(conn, :index))
  end
  defp authenticate_reply(:error, conn) do
    redirect(conn, to: Routes.auth_path(conn, :login, [error: true]))
  end

  def login(conn, params) do
    render(conn, "login.html", [error_feedback:  error_feedback(params)])
  end

  defp error_feedback(%{"error" => "true"}), do: "Invalid username or password."
  defp error_feedback(_), do: nil

  def logout(conn, params) do
    conn
    |> Guardian.Plug.sign_out(Auth.Guardian, [])
    |> redirect(to: Routes.auth_path(conn, :login))
  end

  def test(conn, params) do
    user = Guardian.Plug.current_resource(conn)
    render(conn, "test.html", current_user: user)
  end
end
