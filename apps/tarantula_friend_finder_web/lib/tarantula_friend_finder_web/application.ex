defmodule TarantulaFriendFinderWeb.Application do
  @moduledoc false

  use Application
  require Logger

  def start(_type, _args) do
    System.get_env("ENVIRONMENT")
    |> configure_environment()

    children = [
      TarantulaFriendFinderWeb.Endpoint,
      # Starts a worker by calling: TarantulaFriendFinderWeb.Worker.start_link(arg)
      # {TarantulaFriendFinderWeb.Worker, arg},
    ]

    opts = [strategy: :one_for_one, name: TarantulaFriendFinderWeb.Supervisor]
    Supervisor.start_link(children, opts)
  end

  defp configure_environment(nil), do: :ok
  defp configure_environment(env) do
    # add environment to logger contexts
    configure_endpoint()
    configure_guardian()
  end

  defp configure_endpoint do
    port = System.get_env("PHX_PORT")
    hostname = System.get_env("PHX_HOSTNAME")
    secret_key_base = System.get_env("PHX_SECRET_KEY_BASE")

    Logger.info("Configuring", port: port, hostname: hostname, module: TarantulaFriendFinderWeb.Endpoint)

    old_config = Application.get_env(:tarantula_friend_finder_web, TarantulaFriendFinderWeb.Endpoint)
    new_config = Keyword.merge(old_config, [
      http: [port: port],
      url: [host: hostname, port: port],
      secret_key_base: secret_key_base,
    ])
    Application.put_env(:tarantula_friend_finder_web, TarantulaFriendFinderWeb.Endpoint, new_config, [persistent: true])
  end

  defp configure_guardian do
    secret_key = System.get_env("GRD_SECRET_KEY")

    Logger.info("Configuring", module: TarantulaFriendFinderWeb.Authentication.Guardian)

    old_config = Application.get_env(:tarantula_friend_finder_web, TarantulaFriendFinderWeb.Authentication.Guardian)
    new_config = Keyword.merge(old_config, [
      secret_key: secret_key
    ])
    Application.put_env(:tarantula_friend_finder_web, TarantulaFriendFinderWeb.Authentication.Guardian, new_config, [persistent: true])
  end

  # Tell Phoenix to update the endpoint configuration
  # whenever the application is updated.
  def config_change(changed, _new, removed) do
    TarantulaFriendFinderWeb.Endpoint.config_change(changed, removed)
    :ok
  end
end
