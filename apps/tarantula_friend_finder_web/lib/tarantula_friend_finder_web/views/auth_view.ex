defmodule TarantulaFriendFinderWeb.AuthView do
  use TarantulaFriendFinderWeb, :view

  def form_class(nil), do: "text-left needs-validation"
  def form_class(_), do: "text-left"

  def input_class(nil), do: "form-control"
  def input_class(_), do: "form-control is-invalid"
end
