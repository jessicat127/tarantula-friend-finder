defmodule TarantulaFriendFinderWeb.ErrorView do
  use TarantulaFriendFinderWeb, :view

  def render("400.json", %{reason: reason}) do
    %{
      error: "bad request",
      reason: reason
    }
  end

  def render("401.json", %{body: body}) do
    %{
      error: body
    }
  end

  def render("401.json", _assigns) do
    %{
      error: "unauthorized"
    }
  end

  def render("403.json", _assigns) do
    %{
      error: "forbidden"
    }
  end

  def render("404.json", _assigns) do
    %{
      error: "not found"
    }
  end

  # By default, Phoenix returns the status message from
  # the template name. For example, "404.html" becomes
  # "Not Found".
  def template_not_found(template, _assigns) do
    Phoenix.Controller.status_message_from_template(template)
  end
end
