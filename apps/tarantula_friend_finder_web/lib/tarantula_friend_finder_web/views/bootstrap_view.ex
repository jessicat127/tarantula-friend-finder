defmodule TarantulaFriendFinderWeb.BootstrapView do
  use TarantulaFriendFinderWeb, :view
  import Phoenix.HTML

  @doc """
  CSS input class.  Can provide a feedback `String.t` in order to set the "is-invalid" class
  when said `feedback` is not `nil`.
  """
  def input_class(feedback \\ nil)
  def input_class(nil), do: "form-control"
  def input_class(_feedback), do: "form-control is-invalid"

  @doc """
  Adds a CSS class for invalid form input feedback text if any feedback is actually given.
  Can be used on a fresh form with a value of `nil` when no feedback will be displayed.
  """
  def feedback_class(feedback \\ nil)
  def feedback_class(nil), do: ""
  def feedback_class(_feedback), do: "invalid-feedback"
end
