defmodule TarantulaFriendFinderWeb.LayoutView do
  use TarantulaFriendFinderWeb, :view

  def current_user(conn) do
    Guardian.Plug.current_resource(conn)
  end
end
