defmodule TarantulaFriendFinderWeb.Commanders.CommanderHelpers do

  def get_current_user(socket) do
    token = Drab.Core.get_session(socket, :guardian_default_token)
    Guardian.resource_from_token(TarantulaFriendFinderWeb.Authentication.Guardian, token)
  end
end
