defmodule TarantulaFriendFinderWeb.UserCommander do
  use TarantulaFriendFinderWeb, :commander
  alias TarantulaFriendFinderWeb.{Router, Endpoint}

  ###
  # Action: :new
  # Template: new.html.drab
  ###

  defhandler create_user(socket, sender) do
    poke!(socket, [
      username_feedback: nil,
      password_feedback: nil,
      password_confirmation_feedback: nil,
      email_feedback: nil,
      email_confirmation_feedback: nil,
      loading: true
    ])

    Map.get(sender, :params, %{})
    |> Identity.create_user()
    |> create_user_response(socket)
  end

  defp create_user_response({:ok, user}, socket) do
    p = Router.Helpers.auth_path(Endpoint, :login)
    Drab.Browser.redirect_to(socket, p)
  end
  defp create_user_response({:error, errors}, socket) do
    assigns = Enum.map(errors, fn {field, e} ->
      feedback = Enum.join(e, ", ")
      assign = String.to_existing_atom("#{field}_feedback")
      {assign, feedback}
    end)
    poke!(socket, [{:loading, false}|assigns])
  end

  ###
  # Action: :edit
  # Template: edit.html.drab
  ###

  defhandler swap_edit_partial(socket, sender) do
    current_state = peek!(socket, :edit_partial)
    next_state = get_in(sender, ["data", "name"]) |> String.to_existing_atom()
    case {current_state, next_state} do
      {same_state, same_state} -> :ok
      _ -> poke!(socket, [edit_partial: next_state])
    end
  end

  defhandler change_password(socket, sender) do
    poke!(socket, "edit.change_password_partial.html", [
      loading: true,
      success: false,
      current_password_feedback: nil,
      password_feedback: nil,
      password_confirmation_feedback: nil
    ])

    {:ok, user, _claims} = get_current_user(socket)
    password = sender.params["current_password"]

    Identity.checked_change_password(user, password, sender.params)
    |> change_password_response(socket)
  end

  defp change_password_response({:ok, user}, socket) do
    poke!(socket, "edit.change_password_partial.html", [
      loading: false,
      current_password_feedback: nil,
      password_feedback: nil,
      password_confirmation_feedback: nil,
      success: true
    ])
  end
  defp change_password_response({:error, errors}, socket) do
    assigns = Enum.map(errors, fn
      {:current_password, e} ->
        {:current_password_feedback, Enum.join(e, ", ")}
      {field, e} ->
        {String.to_existing_atom("#{field}_feedback"), Enum.join(e, ", ")}
    end)
    poke!(socket, "edit.change_password_partial.html", [{:loading, false}|assigns])
  end

  defhandler change_email(socket, sender) do

  end

  defhandler i_consent(socket, sender) do
    socket
  end

  defhandler delete_account(socket, sender) do
    poke!(socket, "edit.delete_account_partial.html", [
      loading: true,
      password_feedback: nil
    ])

    warning = """
    Deleting your account will permanently remove all of your data from the website.
    Your account can not be recovered under any circumstances.
    """
    alert(socket, "Warning:", warning, buttons: [ok: "I understand", cancel: "Cancel"])
    |> maybe_delete_account(socket, sender)
  end

  defp maybe_delete_account({:cancel, _}, socket, _sender) do
    poke!(socket, "edit.delete_account_partial.html", [{:loading, false}])
  end
  defp maybe_delete_account({:ok, _}, socket, sender) do
    IO.inspect(sender.params)
    {:ok, user, _claims} = get_current_user(socket)
    password = sender.params["password"] || "" |> IO.inspect()
    assigns = case Identity.delete_user(user, password) do
      :error ->
        poke!(socket, "edit.delete_account_partial.html", [loading: false, password_feedback: "is invalid"])
      :ok ->
        p = Router.Helpers.auth_path(Endpoint, :logout)
        Drab.Browser.redirect_to(socket, p)
    end
  end
end
