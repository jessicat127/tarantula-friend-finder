defmodule TarantulaFriendFinderWeb.SearchCommander do
  use TarantulaFriendFinderWeb, :commander
  alias TarantulaFriendFinderWeb.{Router, Endpoint, BootstrapView, SearchView}

  onload :page_loaded

  def page_loaded(socket) do
    html = render_to_string(SearchView, "classification_partial.html", [
      genus_list: [%{id: 1, name: "Lasiodora"}],
      species_list: nil
    ])
    poke!(socket, form_partial: html)
  end

  defhandler genus_select(socket, sender) do
    genus_id = sender.params["genus"]
    species = get_species(genus_id)

    poke!(socket, "classification_partial.html", species_list: species)
    delete!(socket, attr: "disabled",  from: "#species")
  end

  def get_species(genus_id) do
    [
      %{id: "1", name: "Parahybana"},
      %{id: "2", name: "Klugi"},
      %{id: "3", name: "Difficilis"},
    ]
  end

  defhandler species_select(socket, sender) do
    put_store(socket, :genus_id, sender.params["genus"])
    put_store(socket, :species_id, sender.params["species"])
    delete!(socket, attr: "disabled", from: ".btn-next")
  end

  defhandler next(socket, sender) do
    html = render_to_string(SearchView, "sex_partial.html", [])
    poke!(socket, form_partial: html)
  end

  defhandler submit(socket, sender) do
    sex = sender["val"]
    genus_id = get_store(socket, :genus_id)
    species_id = get_store(socket, :species_id)
    IO.inspect(genus_id, label: "Genus")
    IO.inspect(species_id, label: "Species")
    IO.inspect(sex, label: "Sex")
  end
end
