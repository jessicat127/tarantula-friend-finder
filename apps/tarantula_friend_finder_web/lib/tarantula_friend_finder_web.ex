defmodule TarantulaFriendFinderWeb do
  @moduledoc """
  The entrypoint for defining your web interface, such
  as controllers, views, channels and so on.

  This can be used in your application as:

      use TarantulaFriendFinderWeb, :controller
      use TarantulaFriendFinderWeb, :view

  The definitions below will be executed for every view,
  controller, etc, so keep them short and clean, focused
  on imports, uses and aliases.

  Do NOT define functions inside the quoted expressions
  below. Instead, define any helper function in modules
  and import those modules here.
  """

  def controller do
    quote do
      use Phoenix.Controller, namespace: TarantulaFriendFinderWeb
      import Plug.Conn
      import TarantulaFriendFinderWeb.Gettext
      alias TarantulaFriendFinderWeb.Router.Helpers, as: Routes
      alias TarantulaFriendFinderWeb.ErrorView
    end
  end

  def commander do
    quote do
      use Drab.Commander, modules: [Drab.Live, Drab.Element, Drab.Modal, Drab.Query]
      alias TarantulaFriendFinderWeb.BootstrapView
      import Phoenix.HTML
      import TarantulaFriendFinderWeb.Commanders.CommanderHelpers

      access_session :guardian_default_token
    end
  end

  def view do
    quote do
      use Phoenix.View, root: "lib/tarantula_friend_finder_web/templates",
                        namespace: TarantulaFriendFinderWeb

      # Import convenience functions from controllers
      import Phoenix.Controller, only: [get_flash: 1, get_flash: 2, view_module: 1]

      # Use all HTML functionality (forms, tags, etc)
      use Phoenix.HTML

      import TarantulaFriendFinderWeb.ErrorHelpers
      import TarantulaFriendFinderWeb.Gettext
      alias TarantulaFriendFinderWeb.Router.Helpers, as: Routes
      alias TarantulaFriendFinderWeb.BootstrapView

      def render_bootstrap(template, assigns) do
        render(BootstrapView, template, assigns)
      end
    end
  end

  def router do
    quote do
      use Phoenix.Router
      import Plug.Conn
      import Phoenix.Controller
    end
  end

  def channel do
    quote do
      use Phoenix.Channel
      import TarantulaFriendFinderWeb.Gettext
    end
  end

  @doc """
  When used, dispatch to the appropriate controller/view/etc.
  """
  defmacro __using__(which) when is_atom(which) do
    apply(__MODULE__, which, [])
  end
end
