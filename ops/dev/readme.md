# Deployment

1. (Only if change was made) `scp docker-compose.yml dev-tff@dev-server:~/`
2. `ssh dev-tff@dev-server`
3. `docker-compose down`
4. `docker-compose up --force-recreate --detach`